LIBRARY ieee;
USE ieee.std_logic_1164.all;
--USE IEEE.STD_LOGIC_ARITH.all;
USE IEEE.STD_LOGIC_UNSIGNED.all;
USE IEEE.NUMERIC_STD.ALL;

ENTITY pause IS
	PORT
	(
		clock				: 	IN STD_LOGIC ;
		pixel_row_input : IN STD_LOGIC_VECTOR (9 DOWNTO 0);
		pixel_column_input : IN STD_LOGIC_VECTOR (9 DOWNTO 0);


		character_address_output	:	OUT STD_LOGIC_VECTOR (5 DOWNTO 0);
		font_row_output, font_col_output	:	OUT STD_LOGIC_VECTOR (2 DOWNTO 0)
		
		);
END ENTITY pause;

Architecture pau of pause is 
signal address_var,address_hundred_score,address_hundred_time,address_ten_score,address_ten_time,address_one_score,address_one_time, address_level : STD_LOGIC_VECTOR (5 DOWNTO 0);
signal int_score,int_time,int_hundred_score,int_ten_score,int_one_score,int_hundred_time,int_ten_time,int_one_time :  integer:=1;

begin

int_score <= to_integer(unsigned(score_input));

int_hundred_score <= int_score/100;
int_ten_score <= (int_score - int_hundred_score * 100)/10;
int_one_score <= int_score - int_hundred_score * 100 - int_ten_score*10;


--0XX

with int_hundred_score select address_hundred_score<=
				"110000" when 0,
				"110001" when 1,
				"110010" when 2,
				"110011" when 3,
				"110100" when 4,
				"110101" when 5,
				"110110" when 6,
				"110111" when 7,
				"111000" when 8,
				"111001" when 9,
				"100000" when others;
				
--X0X
with int_ten_score select address_ten_score<=
				"110000" when 0,
				"110001" when 1,
				"110010" when 2,
				"110011" when 3,
				"110100" when 4,
				"110101" when 5,
				"110110" when 6,
				"110111" when 7,
				"111000" when 8,
				"111001" when 9,
				"100000" when others;
				
--XX0
with int_one_score select address_one_score<=
				"110000" when 0,
				"110001" when 1,
				"110010" when 2,
				"110011" when 3,
				"110100" when 4,
				"110101" when 5,
				"110110" when 6,
				"110111" when 7,
				"111000" when 8,
				"111001" when 9,
				"100000" when others;
				


conversion: process( pixel_column_input,pixel_row_input)
begin


	if (pixel_column_input >=16)and (pixel_column_input<32)and (pixel_row_input >=16)and (pixel_row_input<32) then
	address_var<= "010011";
	--S
	elsif ((pixel_column_input >=32)and (pixel_column_input<48))and (pixel_row_input >=16)and (pixel_row_input<32) then
	address_var<= "000011";
	--C
	elsif ((pixel_column_input >=48)and (pixel_column_input<64))and (pixel_row_input >=16)and (pixel_row_input<32) then
	address_var<= "001111";
	--O
	elsif ((pixel_column_input >=64)and (pixel_column_input<80))and (pixel_row_input >=16)and (pixel_row_input<32) then
	address_var<= "010010";
	--R
	elsif ((pixel_column_input >=80)and (pixel_column_input<96))and (pixel_row_input >=16)and (pixel_row_input<32) then
	address_var<= "000101";
	--E
	--------------------------------------------------------------------------------------------------------------------
	--Dynamic numbers of the score
	--------------------------------------------------------------------------------------------------------------------
	elsif ((pixel_column_input >=112)and (pixel_column_input<128))and (pixel_row_input >=16)and (pixel_row_input<32) then
	--address_var<= "100000";
	address_var <= address_hundred_score;
	-- 
	elsif ((pixel_column_input >=128)and (pixel_column_input<144))and (pixel_row_input >=16)and (pixel_row_input<32) then
	--address_var<= "110000";
	address_var <= address_ten_score;
	elsif ((pixel_column_input >=144)and (pixel_column_input<160))and (pixel_row_input >=16)and (pixel_row_input<32) then
	--address_var<= "110001";
	address_var <= address_one_score;
	
	---------------------------------------------------------------------------------------------------------------------
	--Time display 
	---------------------------------------------------------------------------------------------------------------------

--	elsif ((pixel_column_input >=286)and (pixel_column_input<302))and (pixel_row_input >=16)and (pixel_row_input<32) then
--	--address_var<= "110000";
--	address_var <= address_hundred_time;
--	elsif ((pixel_column_input >=302)and (pixel_column_input<318))and (pixel_row_input >=16)and (pixel_row_input<32) then
--	--address_var<= "110000";
--	address_var <= address_ten_time;
--	elsif ((pixel_column_input >=318)and (pixel_column_input<334))and (pixel_row_input >=16)and (pixel_row_input<32) then
--	--address_var<= "100000";
--	address_var <= address_one_time;
	
	---------------------------------------------------------------------------------------------------------------------
	--Level display
	---------------------------------------------------------------------------------------------------------------------
--	elsif (pixel_column_input >=512)and (pixel_column_input<528)and (pixel_row_input >=16)and (pixel_row_input<32) then
--	address_var<= "001100";
--	--end if;--L
--	elsif ((pixel_column_input >=528)and (pixel_column_input<544))and (pixel_row_input >=16)and (pixel_row_input<32) then
--	address_var<= "000101";
--	--end if;--E
--	elsif ((pixel_column_input >=544)and (pixel_column_input<560))and (pixel_row_input >=16)and (pixel_row_input<32) then
--	address_var<= "010110";
--	--end if;--V
--	elsif ((pixel_column_input >=560)and (pixel_column_input<576))and (pixel_row_input >=16)and (pixel_row_input<32) then
--	address_var<= "000101";
--	--E
--	elsif ((pixel_column_input >=576)and (pixel_column_input<592))and (pixel_row_input >=16)and (pixel_row_input<32) then
--	address_var<= "001100";
--	--L
--	elsif ((pixel_column_input >=608)and (pixel_column_input<624))and (pixel_row_input >=16)and (pixel_row_input<32) then
--	address_var<= address_level;
--	--Level "X"
--	--End Level display]
	
	else
	address_var<= "100000";--Space for the other situation
	end if;--spoace
	
end process conversion;



character_address_output <= address_var;

font_col_output <= pixel_column_input(3 DOWNTO 1);
font_row_output <= pixel_row_input(3 DOWNTO 1);

end Architecture pau;